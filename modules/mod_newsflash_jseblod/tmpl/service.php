<?php defined('_JEXEC') or die('Restricted access');

?>

<section>
	<div class="container">
		<div class="row text-center">
			<?php for($i=0; $i < count($list); $i++){ ?>
				<?php modNewsFlashjSeblodHelper::renderItem($list[$i], $params, $access); ?>
			<?php } ?>
		</div>
	</div>
</section>
