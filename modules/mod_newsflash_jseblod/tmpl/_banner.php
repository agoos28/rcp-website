<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 

?>
<?php echo $item->editlink; ?>
<section class="imagebg height-50 parallax" data-overlay="0">
	<div class="background-image-holder">
		<img alt="image" src="<?php echo $item->rawcontent->img; ?>">
	</div>
	<div class="container pos-vertical-center">
		<div class="row">
			<div class="col-sm-12 color--white--forced">
				<?php echo $item->rawcontent->wysiwyg_text; ?>
			</div>
		</div>
	</div>
</section>