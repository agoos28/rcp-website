<?php // no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.filteroutput');


?>

<ul class="slides">
	<?php for ($i = 0, $n = count($item->rawcontent->img_group); $i < $n; $i ++) : ?>
		<li
			class="imagebg"
			data-overlay="1"
			style="height: calc(100vw * 0.4); min-height: 300px; max-height: 500px"
		>
			<div class="background-image-holder">
				<img
					alt="<?php echo $item->rawcontent->img_group[$i]['alt']; ?>"
					src="<?php echo $item->rawcontent->img_group[$i]['img']; ?>"
				/>
			</div>
			<div class="container pos-vertical-center">
				<div class="row">
					<div class="col-md-6 col-sm-8 col-xs-12">
						<?php echo $item->rawcontent->img_group[$i]['wysiwyg_description']; ?>
					</div>
				</div>
			</div>
		</li>
	<?php endfor; ?>
</ul>
