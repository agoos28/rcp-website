<?php // no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div class="col-sm-4">
	<div class="feature feature-1">
		<a href="<?php echo $item->linkOn;?>" class="block m-b-30">
			<i class="icon icon--lg <?php echo $item->rawcontent->icon; ?>"></i>
			<h4><?php echo $item->rawcontent->content_title; ?></h4>
		</a>
		<div class="text-left">
			<?php echo $item->rawcontent->list_view_text; ?>
		</div>
	</div>
</div>
