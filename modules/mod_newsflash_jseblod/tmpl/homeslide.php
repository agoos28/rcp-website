<?php defined('_JEXEC') or die('Restricted access');


?>
<section
		class="slider slider--animate overlay--rip"
		style="height: calc(100vw * 0.4); min-height: 300px; max-height: 500px"
		data-animation="fade"
		data-arrows="true"
		data-paging="true" data-timing="5000"
>
	<?php modNewsFlashjSeblodHelper::renderItem($list[0], $params, $access); ?>
</section>
<div style="height: calc(6vw)"></div>