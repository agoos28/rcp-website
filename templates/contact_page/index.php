<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Contact Page Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>

<style>
	footer::before {
		opacity: 0.85;
	}
</style>

<section class="map-1">
	<div class="map-container">
		<iframe src="<?php echo $jSeblod->iframe->value; ?>"></iframe>
	</div>
</section>

<section class="bg--secondary">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="feature feature-3 m-t-40 m-t-sm-0">
					<div class="feature__left">
						<i class="icon icon-Phone"></i>
					</div>
					<div class="feature__right">
						<?php echo $jSeblod->content_text->value; ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<form action="<?php echo JURI::base(); ?>submit-contact" class="form--square form-email" method="post" name="emailForm" id="ckform">

					<div class="input-with-icon col-sm-12">
						<i class="icon-MaleFemale"></i>
						<input class="validate-required" type="text" name="name" placeholder="Your Name">
					</div>
					<div class="input-with-icon col-sm-6">
						<i class="icon-Email"></i>
						<input class="validate-required validate-email" type="email" name="email" placeholder="Email Address">
					</div>
					<div class="input-with-icon col-sm-6">
						<i class="icon-Phone-2"></i>
						<input type="tel" name="telephone" placeholder="Phone Number">
					</div>
					<div class="col-sm-12">
						<textarea class="validate-required" name="message" placeholder="Your Message" rows="8"></textarea>
					</div>
					<div class="col-sm-12">
						<button type="submit" class="btn btn--primary vpe">
							Submit Form
						</button>
					</div>
					<input type="hidden" name="view" value="contact" />
					<input type="hidden" name="id" value="2" />
					<input type="hidden" name="task" value="submit" />
					<?php echo JHTML::_( 'form.token' ); ?>
				</form>
			</div>
		</div>
	</div>
</section>