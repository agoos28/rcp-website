<?php // @version $Id: blog.php 9722 2007-12-21 16:55:15Z mtk $
defined('_JEXEC') or die('Restricted access');
$cparams = JComponentHelper::getParams('com_media');
//echo'<pre>';print_r(JURI::current());die();
$categories = $this->get('Categories');
$section = $this->get('Section');

$metadesc = '';
$metakey = $this->params->get('page_title');

?>
<?php if ($user->usertype == 'Super Administrator') { ?>
<div class="container pos-relative">
	<?php echo '<a style="z-index:3;" class="btn btn-edit" href="index.php?option=com_cckjseblod&view=type&layout=form&typeid=28&cckid=' . $this->category->id . '">edit</a>'; ?>
</div>
<?php } ?>

<?php if ($this->params->get('show_page_title', 0)) { ?>
<section class="height-50 page-title page-title--animate">
	<div class="container pos-vertical-center">
		<div class="row gutter-10">
			<div class="col-sm-12 text-center">
				<h1>
					<?php echo $metakey; ?>
				</h1>
				<?php if ($this->params->get('show_description', 0)) { ?>
					<div class="lead"><?php echo $this->category->description; ?></div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<section class="masonry-contained">
	<div class="container">
		<div class="row gutter-20">
			<div class="masonry masonry-shop">
				<div class="masonry__container masonry--animate">
					<?php $introcount = $this->params->get('num_intro_articles', 12);
					if ($introcount) :
						$colcount = ceil(12 / $this->params->get('num_columns', 1));
					if ($colcount == 0) : $colcount = 1; endif; ?>
					<?php for ($i = $this->pagination->limitstart; $i < $this->total; $i++) : ?>
					<div class="<?php echo $this->params->get('col_class', 'col-sm-4'); ?> masonry__item">
						<?php $this->item = &$this->getItem($i, $this->params);
						echo $this->loadTemplate('item'); ?>
					</div>
					<?php endfor; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($this->pagination->get('pages.total') > 1) : ?>
		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
		<?php echo $this->pagination->getPagesLinks(); ?>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</section>
<?php 
$doc = &JFactory::getDocument();
$metadesc = $this->params->get('page_title');
//$doc->setMetaData( 'description',  $metadesc);
//$doc->setMetaData( 'keywords',  $metakey);
//$doc->setBuffer('Baju Anak Branded Kategori '.$this->params->get('page_title'), 'seotitle', 'value');
?>