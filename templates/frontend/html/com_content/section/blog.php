<?php // @version $Id: blog.php 9722 2007-12-21 16:55:15Z mtk $
defined('_JEXEC') or die('Restricted access');
$cparams = JComponentHelper::getParams('com_media');
//echo'<pre>';print_r(JURI::current());die();
$categories = $this->get('Categories');
$section    = $this->get('Section');

$metadesc = '';
$metakey  = $this->params->get('page_title');

?>

<?php
$top_modules =& JModuleHelper::getModules('section-top');
foreach ($top_modules as $top_module)
{
	echo JModuleHelper::renderModule($top_module);
}
?>
<section>
	<?php if ($this->params->get('show_page_title', 0)) { ?>
		<h3 style="color: #333;font-weight: 300;text-transform: uppercase;"><?php echo $metakey; ?></h3>
	<?php } ?>
	<?php if ($this->params->get('show_description', 0)) { ?>
		<?php if ($user->usertype == 'Super Administrator')
		{
			$link = "index.php?option=com_cckjseblod&view=type&layout=form&typeid=28&cckid=" . $this->category->id; ?>
			<div class="pos-relative">
				<a style="z-index:3;" class="btn btn-edit" href="<?php echo $link; ?>">edit</a>
			</div>
		<?php } ?>
		<?php echo $this->category->description; ?>
	<?php } ?>
	<div>
		<?php $introcount = $this->params->get('num_intro_articles', 12);
		if ($introcount) :
			$colcount = ceil(12 / ($this->params->get('num_columns', 1) || 1));
			if ($colcount == 0) :
				$colcount = 1;
			endif; ?>
			<div class="row <?php echo $this->params->get('row_class'); ?>">
				<?php for ($i = $this->pagination->limitstart; $i < $this->total; $i++) : ?>
					<div class="<?php echo $this->params->get('col_class', 'col-xs-12'); ?>">
						<?php $this->item  = &$this->getItem($i, $this->params);
						$this->item->index = $i;
						echo $this->loadTemplate('item'); ?>
					</div>
				<?php endfor; ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($this->pagination->get('pages.total') > 1) : ?>
		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<?php echo $this->pagination->getPagesLinks(); ?>
		<?php endif; ?>
	<?php endif; ?>
</section>

<?php
$bottom_modules =& JModuleHelper::getModules('section-bottom');
foreach ($bottom_modules as $bottom_module)
{
	echo JModuleHelper::renderModule($bottom_module);
}

$doc      = &JFactory::getDocument();
$metadesc = $this->params->get('page_title');
//$doc->setMetaData( 'description',  $metadesc);
//$doc->setMetaData( 'keywords',  $metakey);
//$doc->setBuffer('Baju Anak Branded Kategori '.$this->params->get('page_title'), 'seotitle', 'value');
?>
