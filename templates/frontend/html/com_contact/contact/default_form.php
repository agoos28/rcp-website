<?php // @version $Id: default_form.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
$user	= JFactory::getUser();
?>



<section class="bg--secondary">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="feature feature-3 m-t-40 m-t-sm-0">
					<div class="feature__left">
						<i class="icon icon-Phone"></i>
					</div>
					<div class="feature__right">
						<h4>Reach us right here…</h4>
						<p>Grok integrate thinker-maker-doer piverate entrepreneur sticky note iterate waterfall is so 2000 and late 360 campaign cortado quantitative vs. qualitative.</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<form action="<?php echo JURI::current(); ?>" class="form--square form-email" method="post" name="emailForm" id="ckform">

					<div class="input-with-icon col-sm-12">
						<i class="icon-MaleFemale"></i>
						<input class="validate-required" type="text" name="name" placeholder="Your Name">
					</div>
					<div class="input-with-icon col-sm-6">
						<i class="icon-Email"></i>
						<input class="validate-required validate-email" type="email" name="email" placeholder="Email Address">
					</div>
					<div class="input-with-icon col-sm-6">
						<i class="icon-Phone-2"></i>
						<input type="tel" name="telephone" placeholder="Phone Number">
					</div>
					<div class="col-sm-12">
						<textarea class="validate-required" name="message" placeholder="Your Message" rows="8"></textarea>
					</div>
					<div class="col-sm-12">
						<button type="submit" class="btn btn--primary vpe">
							Submit Form
						</button>
					</div>
					<input type="hidden" name="view" value="contact" />
					<input type="hidden" name="id" value="<?php echo $this->contact->id; ?>" />
					<input type="hidden" name="task" value="submit" />
					<?php echo JHTML::_( 'form.token' ); ?>
				</form>
			</div>
		</div>
	</div>
</section>
