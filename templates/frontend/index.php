<?php
defined('_JEXEC') or die('Restricted access');
$curentUrl = JURI::getInstance();
$baseUrl   = JURI::base();
$tmplUrl   = $baseUrl . 'templates/frontend/';
$conf      = &JFactory::getConfig();
$sitename  = $conf->getValue('config.sitename');
$user      = JFactory::getUser();
//remove mootools.js and caption.js
$headerstuff = $this->getHeadData();

reset($headerstuff['styleSheets']);
foreach ($headerstuff['styleSheets'] as $key => $value)
{
	unset($headerstuff['styleSheets'][$key]);
}
reset($headerstuff['scripts']);
foreach ($headerstuff['scripts'] as $key => $value)
{
	unset($headerstuff['scripts'][$key]);
}
reset($headerstuff['script']);
foreach ($headerstuff['script'] as $key => $value)
{
	unset($headerstuff['script'][$key]);
}
$this->setHeadData($headerstuff);
$this->setGenerator('agoos28');

$menu   = &JSite::getMenu();
$ishome = false;
if ($menu->getActive() == $menu->getDefault())
{
	$ishome = true;
	$jquery = 'jquery.112.js?v=3';
}
else
{
	$jquery = 'jquery.js?v=3';
}
if ($headerstuff['metaTags']['standard']['title'])
{
	$this->setTitle($mainframe->getCfg('sitename') . ' - ' . $headerstuff['metaTags']['standard']['title']);
}
else
{
	$this->setTitle($mainframe->getCfg('sitename') . ' - ' . $this->getTitle());
}
if (!trim($this->getDescription()))
{
	$this->setDescription($mainframe->getCfg('MetaDesc'));
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
			lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta charset="utf-8" content="text/html">
	<jdoc:include type="head"/>
	<link rel="stylesheet" media="screen" href="<?php echo $tmplUrl; ?>css/master.css?ver=4"/>
	<link rel="stylesheet" media="screen" href="<?php echo $tmplUrl; ?>css/utilities.css?ver=1"/>
	<link rel="stylesheet" media="screen" href="<?php echo $tmplUrl; ?>css/theme-custom.css?ver=1"/>
	<link rel="stylesheet" media="screen" href="<?php echo $tmplUrl; ?>css/responsive.css?ver=1"/>
	<script src="<?php echo $tmplUrl; ?>js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		var baseUrl = "<?php echo $baseUrl ?>"
		var currentUrl = "<?php echo urlencode(JURI::current()); ?>"
		var curlang = "<?php echo $this->language; ?>"
	</script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body data-reveal-selectors="section:not(.masonry):not(:first-of-type):not(.parallax)">
<div class="nav-container">
	<nav>
		<div class="nav-bar" data-fixed-at="120">
			<div class="container">
				<div class="nav-module logo-module left">
					<a href="<?php echo $baseUrl ?>">
						<img alt="logo" class="logo logo-dark" src="<?php echo $baseUrl ?>images/logo.png">
					</a>
				</div>
				<div class="nav-module menu-module left">
					<jdoc:include type="modules" name="mainmenu"/>
				</div>
				<div class="nav-module logo-module right">
					<a href="<?php echo $baseUrl ?>">
						<img alt="logo" class="emblem logo-dark" src="<?php echo $baseUrl ?>images/agents.png">
					</a>
				</div>
			</div>
		</div>
		<div class="nav-mobile-toggle visible-sm visible-xs">
			<i class="icon-Align-JustifyAll icon icon--sm"></i>
		</div>
	</nav>
</div>
<div class="fullscreen-container">
	<jdoc:include type="modules" name="top"/>
</div>
<div class="main-container">
	<jdoc:include type="modules" name="main-top"/>
	<jdoc:include type="component"/>
	<jdoc:include type="modules" name="main-bottom"/>
</div>
<div class="fullscreen-container">
	<jdoc:include type="modules" name="bottom"/>
</div>
<footer class="footer-2 text-center-xs bg--dark">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<img alt="logo" class="logo-footer" src="<?php echo $baseUrl ?>images/logo.png">
			</div>
			<div class="col-sm-8 text-right text-center-xs">
				<img alt="bureau-veritas" class="certs" src="<?php echo $baseUrl ?>images/veritas.png">
				<img alt="iso-9001" class="certs" src="<?php echo $baseUrl ?>images/iso.png">
			</div>
			<div class="col-sm-12">
				<jdoc:include type="modules" name="mainmenu"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6 text-right text-center-xs">
				<div class="footer__lower">
					<a class="type--fine-print" style="margin-right: 16px;">Privacy Policy</a>
					<a class="type--fine-print">Term of use</a>
				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6 text-center-xs">
				<div class="footer__lower">
					<span class="type--fine-print">© 2021 PT. Rahayu Carita Perkasa. All Rights Reserved</span>
				</div>
			</div>
		</div>
	</div>
</footer>
<script src="<?php echo $tmplUrl; ?>js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo $tmplUrl; ?>js/scrollreveal.min.js" type="text/javascript"></script>
<script src="<?php echo $tmplUrl; ?>js/parallax.js" type="text/javascript"></script>
<script src="<?php echo $tmplUrl; ?>js/scripts.js" type="text/javascript"></script>
</body>

</html>