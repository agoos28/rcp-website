<?php
/**
 * @version       1.9.0
 * @author        http://www.seblod.com
 * @copyright     Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see _LICENSE.php
 * @package       Product Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod = clone $this;
?>

<?php
/**
 * Init Style Parameters
 **/
include(dirname(__FILE__) . '/params.php');
?>


<section class="imagebg height-60" data-overlay="0">
	<div class="background-image-holder">
		<img alt="<?php echo $jSeblod->content_title->value; ?>" src="<?php echo $jSeblod->img->value; ?>">
	</div>
	<div class="container pos-vertical-center">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1><br></h1>
				<p class="lead"><br></p>
			</div>
		</div>
	</div>
</section>

<section class="height-30 bg--secondary">
	<div class="container pos-vertical-center">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2><?php echo $jSeblod->content_title->value; ?></h2>
				<span><em><?php echo $this->content->section; ?> / <?php echo $this->content->category; ?></em></span>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<?php echo $jSeblod->content_text->value; ?>
			</div>
		</div>
	</div>
</section>

<section class="cta cta-2 bg--white">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="cta__body bg--white">
					<div class="cta__feature">
						<i class="icon icon-Speach-Bubbles icon--sm"></i>
						<span class="h6">WA 0812-345-678</span>
					</div>
					<div class="cta__feature">
						<i class="icon icon-Alarm icon--sm"></i>
						<span class="h6">Open 09:00-18:00</span>
					</div>
					<div class="cta__feature">
						<i class="icon icon-Spot icon--sm"></i>
						<span class="h6">Jakarta. Indonesia</span>
					</div>
					<a class="btn btn--primary vpe" href="#purchase-template">
						<span class="btn__text">Contact us for more</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

