<?php
/**
 * @version       1.9.0
 * @author        http://www.seblod.com
 * @copyright     Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see _LICENSE.php
 * @package       Product Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod = clone $this;
?>

<?php
/**
 * Init Style Parameters
 **/
include(dirname(__FILE__) . '/params.php');
$index = $this->content->index;
?>

<section class="container imageblock">
	<div class="imageblock__content col-md-6 col-sm-6 pos-<?php echo ($index % 2) ? 'right' : 'left' ?>">
		<div class="background-image-holder">
			<img alt="<?php echo $jSeblod->content_title->value; ?>" src="<?php echo $jSeblod->img->value; ?>">
		</div>
	</div>
	<div class="container bg--dark p-t-xs-30">
		<div class="row">
			<div class="col-md-4 <?php echo ($index % 2) ? 'col-sm-4 col-sm-push-1' : 'col-md-push-7 col-sm-4 col-sm-push-7' ?>">
				<h4><?php echo $jSeblod->content_title->value; ?></h4>
				<?php echo $jSeblod->list_view_text->value; ?>
				<div>
					<a class="btn btn--xs btn--primary" href="<?php echo $this->content->art_link; ?>">
						<span class="btn__text">Go to details</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
